ImixsCrypt
==========

ImixsCrypt provides you an open, secure and transparent API to your privacy.

Your privacy
============

We think that your privacy is one of the most valuable things when your are using the Internet. 
ImixsCrypt gives you back the control of your privacy.

Don't trust
============

You should not trust any servers in the Internet which you have no control about. ImixsCrypt gives you a private local webserver. ImixsCrypt only runs on your local machine which you should trust in.

Keep your secrets
============

There is no reason to trust anyone in the Internet your secrets or your private password. Don't believe that this is necessary. With ImixsCrypt there is an easy solution to safely navigate the Internet.

Use Cryptography
============
Cryptography is nothing new. It exitss before the internet becomes part of your live. Imixs crypt uses a well known and trustable crypthography alogrithm called RSA. Read was RSA is and how it works.
